CC = gcc
CFLAGS = -g -I./
LDLIBS = -lSDL2 -lSDL2_image -lGL -lX11 -lpthread -lXrandr -lXi -ldl -lm -lglfw

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
UTIL_SRCS=$(wildcard utils/*.c)
UTIL_OBJS=$(UTIL_SRCS:.c=.o)

opengl: $(OBJS) $(UTIL_OBJS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o ./opengl

clean:
	rm -f opengl utils/*.o
