#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include "utils/window.h"
#include "utils/file.h"
#include "utils/shader.h"

void initializeGlfw();
void initializeGlad();
void processInput(GLFWwindow *window);

int
main()
{
	initializeGlfw();

	GLFWwindow *window;
	window = loadWindow(500, 500, "Hello World");

	initializeGlad();

	// vertices and indices for rendering a diamond shape
	float vertices[] =
	{
		 0.0f,  0.0f, 0.0f, // center 0
		 0.0f,  0.8f, 0.0f, // top 1
		 0.0f, -0.8f, 0.0f, // bottom 2
		-0.8f,  0.0f, 0.0f, // left 3
		 0.8f,  0.0f, 0.0f, // right 4
	};
	unsigned int indices[] =
	{
		0, 1, 3, // top-left triangle
		0, 1, 4, // top-right triangle
		0, 2, 3, // bottom-left triangle
		0, 2, 4, // bottom-right triangle
	};

	const char *vertexSource;
	vertexSource = getFileContentsAsString("shaders/vertex.glsl", 200);

	const char *fragmentSource;
	fragmentSource = getFileContentsAsString("shaders/fragment.glsl", 200);

	uint32_t shaderProgram;
	shaderProgram = compileShaderProgramFromSources(vertexSource, fragmentSource);

	uint32_t VBO, VAO, EBO;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    glDeleteProgram(shaderProgram);

	glfwTerminate();
	return 0;
}

void
initializeGlfw()
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
}

void
initializeGlad()
{
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		exitWithError("Failed to initialize GLAD");
}

void
processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}
