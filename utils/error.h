#pragma once

#include "GLFW/glfw3.h"

void exitWithGlfwError();
void exitWithShaderError(uint32_t shader, int32_t type);
void exitWithShaderProgramError(uint32_t program);
void exitWithError(const char *message);
