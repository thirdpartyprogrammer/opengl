#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <memory.h>

#include "file.h"
#include "string.h"

char *
getFileContentsAsString(const char *filename, size_t length)
{
	FILE *fp;
	char *string;

	fp = openReadonlyFile(filename);
    string = allocateHeapString(length);

	fread(string, 1, length, fp);

	return string;
}

static FILE *
openReadonlyFile(const char *filename)
{
	FILE *fp;
	fp = fopen(filename, "r");

	if (fp == NULL)
	{
		fprintf(stderr, "File '%s' could not be found.\n", filename);
		exit(2);
	}

	return fp;
}
