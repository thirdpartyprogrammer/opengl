#pragma once

#include <GLFW/glfw3.h>

#include "error.h"

GLFWwindow *loadWindow(uint32_t width, uint32_t height, const char *title);
static void framebufferSizeCallback(GLFWwindow *window, int width, int height);
