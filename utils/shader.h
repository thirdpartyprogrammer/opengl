#pragma once

#include <stdbool.h>

#include <GLFW/glfw3.h>

uint32_t compileShaderProgramFromSources(const char *vertexSource, const char *fragmentSource);
uint32_t compileShaderProgramFromShaders(uint32_t vertexShader, uint32_t fragmentShader);
static bool shaderProgramHasCompileError(uint32_t program);

uint32_t compileShader(const char *shaderSource, int32_t type);
static uint32_t createShaderFromSource(const char *source, int32_t type);
static bool shaderHasCompileError(uint32_t shader);
