#include <stdio.h>
#include <stdlib.h>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include "error.h"

void
exitWithGlfwError()
{
	const char *message;

	glfwGetError(&message);
	exitWithError(message);
}

void
exitWithShaderError(uint32_t shader, int32_t type)
{
	if (type == GL_VERTEX_SHADER)
		printf("%s\n", "Error with vertex shader:");
	else if (type == GL_FRAGMENT_SHADER)
		printf("%s\n", "Error with fragment shader:");

	char infoLog[512];
	glGetShaderInfoLog(shader, 512, NULL, infoLog);

	exitWithError(infoLog);
}

void
exitWithShaderProgramError(uint32_t program)
{
	char infoLog[512];
	glGetProgramInfoLog(program, 512, NULL, infoLog);
}

void
exitWithError(const char *message)
{
	fprintf(stderr, "%s\n", message);
	exit(EXIT_FAILURE);
}
