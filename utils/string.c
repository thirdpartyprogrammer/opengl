#include <stdlib.h>

#include "string.h"

char *
allocateHeapString(size_t length)
{
	return (char *)calloc(length, sizeof(char));
}
