#pragma once

#include <stdint.h>

char *getFileContentsAsString(const char *filename, size_t length);
static FILE *openReadonlyFile(const char *filename);
