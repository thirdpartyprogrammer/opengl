#include <stdbool.h>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include "shader.h"
#include "error.h"

uint32_t
compileShaderProgramFromSources(const char *vertexSource, const char *fragmentSource)
{
	uint32_t vertexShader, fragmentShader;

	vertexShader = compileShader(vertexSource, GL_VERTEX_SHADER);
	fragmentShader = compileShader(fragmentSource, GL_FRAGMENT_SHADER);

	uint32_t program;
	program = compileShaderProgramFromShaders(vertexShader, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return program;
}

uint32_t
compileShaderProgramFromShaders(uint32_t vertexShader, uint32_t fragmentShader)
{
	uint32_t program;
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	if (shaderProgramHasCompileError(program))
		exitWithShaderProgramError(program);

	return program;
}

static bool
shaderProgramHasCompileError(uint32_t program)
{
	int32_t success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);

	return !(bool)success;
}

uint32_t
compileShader(const char *source, int32_t type)
{
	uint32_t shader;
	shader = createShaderFromSource(source, type);

	if (shaderHasCompileError(shader))
		exitWithShaderError(shader, type);

	return shader;
}

static uint32_t
createShaderFromSource(const char *source, int32_t type)
{
	uint32_t shader;
	shader = glCreateShader(type);

	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	return shader;
}

static bool
shaderHasCompileError(uint32_t shader)
{
	int32_t success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	return !(bool)success;
}
