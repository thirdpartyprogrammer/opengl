#include <stdint.h>

#include <GLFW/glfw3.h>

#include "window.h"
#include "error.h"

GLFWwindow *
loadWindow(uint32_t width, uint32_t height, const char *title)
{
	GLFWwindow *window;
	window = glfwCreateWindow(width, height, title, NULL, NULL);

	if (window == NULL)
		exitWithGlfwError();

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	return window;
}

static void
framebufferSizeCallback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}
